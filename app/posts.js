var config = require("../config/development.json");
var mysql = require('mysql');
var connection = mysql.createConnection({
   host: config.sql_db.socialTest.host,
   database: config.sql_db.socialTest.database,
   user: config.sql_db.socialTest.user,
   password: config.sql_db.socialTest.password
});

function addPosts(req, res) {
	const title = req.body.title;

	connection.query(`INSERT INTO tweetup.posts (title) VALUES (?)`, [title], function(err, result) {
		if(err) {
			console.log(err);
			res.send(err);
		}	
		else {
			console.log(result);
			res.send(result);
		}
	});
}

function editPosts(req, res) {
	const title = req.body.title;
	const id = req.body.id;

	connection.query(`UPDATE tweetup.posts SET title = ? WHERE id = ?`, [title, id], function(err, result) {
		if(err) {
			console.log(err);
			res.send(err);
		}	
		else {
			console.log(result);
			res.send(result);
		}
	});
}

function deletePosts(req, res) {
	const id = req.body.id;

	connection.query(`DELETE FROM tweetup.posts WHERE id = ?`, id, function(err, result) {
		if(err) {
			console.log(err);
			res.send(err);
		}	
		else {
			console.log(result);
			res.send(result);
		}
	});
}

exports.addPosts = addPosts;
exports.editPosts = editPosts;
exports.deletePosts = deletePosts;