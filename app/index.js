const express = require('express');
const router = express.Router();
const app = express();
const posts = require('./posts.js');
const config = require("../config/development.json");
const bodyParser = require('body-parser')

app.use('/public', express.static(__dirname + '/public'));

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 


router.get('/robot', function(req, res) {
    res.send('Hello World.');
});


router.post('/addPost', posts.addPosts)
router.post('/editPost', posts.editPosts)
router.post('/deletePost', posts.deletePosts)


app.use("/",router);

// Listen to this Port

app.listen(config.port,function(){
  console.log("Server running at Port" + ' ' + config.port);
});  

