var xhttp = new XMLHttpRequest();

window.onload = function() {

	document.getElementById("addPost").addEventListener("submit", function(e) {
		e.preventDefault();
		var title = document.getElementById("title").value;

		xhttp.open("POST", "/addPost", true);
	    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	    xhttp.send('title='+encodeURIComponent(title));
	});
}	  